public class Catch{
  
  private static final double MAX_WEIGHT = 1000; //maximum weight of the catch allowed
  private static final double LEAST_WEIGHT = 0;// minimum weight of catch allowed
  
  private double catchWeight;//weight of the catch
  
  //a constructor that initializes the catches
  public Catch(){
    catchWeight=LEAST_WEIGHT;
  }
  
  //a second construcutor that initializes the catches and assigns a weight
  public Catch(double weight){
    if(weight<LEAST_WEIGHT) 
      System.out.println("invalid weight; weight cannot be negative");
    else if(weight>MAX_WEIGHT)
      System.out.println("invalid weight; weight cannot be greater than 1000");
    else
      catchWeight = weight;
  }
  
  //a method that adds a weight to catch
  public void setWeight(double weight){
    if(weight<LEAST_WEIGHT) 
      System.out.println("invalid weight; weight cannot be negative");
    else if(weight>MAX_WEIGHT)
      System.out.println("invalid weight; weight cannot be greater than 1000");
    else
      catchWeight = weight;
  }
  
  //a method that returns the weight of a catch
  public double getWeight(){
    return this.catchWeight;
  }
  
  
  
}
public class Trawler{
  
  private static final int MAX_CATCH = 100;//maximum number of catches allowed 
  
  private static final int LOWEST_CATCH = 0;//lowest number of catches allowed 
  
  private int numberOfCatches;//instance variable for the number of catches 
  
  private double trawlerWeight;//instance variable for the weight of the trawler
  
  
//a method that returns the current number of catches in the refrigerator 
  public int GetNumCatches(){
    return this.numberOfCatches;
  }

  
//a method that returns the weight of the trawler
  public double getTrawlerWeight(){
    return trawlerWeight;
  }
  
  
  
//a method that increases the number of catches by one
  public void addCatch(Catch fishCatch){
    if(this.numberOfCatches == MAX_CATCH)
      System.out.println("Trawler full");
    else
    this.numberOfCatches++;
    
    trawlerWeight = fishCatch.getWeight();
    
  }
  
  
  //a constructor that initializes a fridge
  public Trawler(){
    numberOfCatches = 0;
    trawlerWeight=0;
  }
  
  
}